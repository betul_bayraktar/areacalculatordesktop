﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AreaCalculator
{
    public partial class Form1 : Form
    {

        public int locx, locy;


        public bool twoPoint = true;
        public int clickCount = 0;
        public int firstlocx, firstlocy, secondlocx, secondlocy;
        public bool calculatePoint = true;
        public int calculatCounts = 0;

        Image File;

        public List<PointF> calculatorPoint = new List<PointF>();

        public float imageh, imagew, panel3h, panel3w;

        public float scalew, scaleh;
        double area;

        PointF input1, input2;

        public float zoom = 1f;
        public bool zoomState = false;

        public string picPath;

        public Form1()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel3.Controls.Clear();
            panel3.BackgroundImage = null;
            panel3.Invalidate();
            ClearEverything();

        }
        public void ClearEverything()
        {
            twoPoint = true;
            clickCount = 0;
            firstlocx = 0;
            firstlocy = 0;
            secondlocx = 0;
            secondlocy = 0;
            calculatePoint = true;
            calculatCounts = 0;
            area = 0;
            calculatorPoint.Clear();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            zoom = Convert.ToSingle(trackBar1.Value)/100;
            zoomState = true;
            panel3.Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show("PLS WRITE INPUT VALUE");
                return;
            }
            calculateArea();



            area = area * scaleh * scalew;

            input1.X = firstlocx;
            input1.Y = firstlocy;
            input2.X = secondlocx;
            input2.Y = secondlocy;

            double inputLineValue = twoPointValue(input1, input2);

            inputLineValue = inputLineValue * scalew;
            double one = Convert.ToDouble(textBox1.Text) / inputLineValue;

            area = area * one * one;


            area = Math.Abs(area);
            area = area / 10000;

            label3.Text = area.ToString();
        }
        public double twoPointValue(PointF point1,PointF point2)
        {
            double value;
            value= Math.Sqrt(Math.Pow((point1.X - point2.X),2)  + Math.Pow((point1.Y-point2.Y),2));
            return value;
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog f = new OpenFileDialog();
                f.Filter = "Image files (*.jpg, *.png) | *.jpg; *.png";

                if (f.ShowDialog() == DialogResult.OK)
                {
                    //if (f.FileName.Contains(" "))
                    //{
                    //    throw new System.ArgumentException("Parameter cannot be space", "original");
                    //}
                    picPath = f.FileName;
                    File = Image.FromFile(f.FileName);
                    setBackground(File);
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {
            /*
            if (zoomState == true)
            {
                Graphics g = e.Graphics;
                g.ScaleTransform(zoom, zoom);
                zoomState = false;
            }*/
            if (clickCount == 3)
            {
                Pen redPen = new Pen(Color.Red, 2);
                e.Graphics.DrawLine(redPen, firstlocx, firstlocy, secondlocx, secondlocy);
            }
            if (calculatePoint == false)
            {
                int pointcount = calculatorPoint.Count();
                if (pointcount > 1)
                {
                    Pen redPen = new Pen(Color.Red, 2);
                    
                    for (int i = 0; i < pointcount-1; i++)
                    {
                        PointF startPoint = new PointF();
                        PointF endPoint = new PointF();
                        startPoint = calculatorPoint[i];
                        endPoint = calculatorPoint[i+1];
                        e.Graphics.DrawLine(redPen, startPoint.X, startPoint.Y, endPoint.X, endPoint.Y);
                    }
                }
            }
        }

        public void setBackground(Image File)
        {
            panel3w = panel3.Width;
            panel3h = panel3.Height;

            imagew = File.Width;
            imageh = File.Height;


            scalew = imagew / panel3w;
            scaleh = imageh / panel3h;

            //Image btmpBackGroun = new Bitmap(picPath);
            //Size panelSize = new Size();
            //panelSize.Width = Convert.ToInt32(panel3w);
            //panelSize.Height = Convert.ToInt32(panel3h);

            //btmpBackGroun = resizeImage(btmpBackGroun, panelSize);


            panel3.BackgroundImage = File;
            panel3.BackgroundImageLayout = ImageLayout.Stretch;
        }
        public static Image resizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            twoPoint = false;
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {
            if (twoPoint == false)
            {
                clickCount += 1;
                if (clickCount == 1)
                {
                    Label firstPoint = new Label();
                    firstPoint.Text = "1";
                    firstPoint.Width = 20;
                    firstPoint.Height = 20;
                    firstPoint.Font= new Font("Arial", 10, FontStyle.Bold);
                    firstPoint.Location = new Point(locx, locy);
                    firstPoint.ForeColor = Color.Red;
                    firstPoint.BackColor = Color.Transparent;
                    firstlocx = locx;
                    firstlocy = locy;
                    panel3.Controls.Add(firstPoint);
                }
                if (clickCount == 2)
                {
                    Label seconfPoint = new Label();
                    seconfPoint.Text = "2";
                    seconfPoint.Width = 20;
                    seconfPoint.Height = 20;
                    seconfPoint.Font = new Font("Arial", 10, FontStyle.Bold);
                    seconfPoint.Location = new Point(locx, locy);
                    seconfPoint.ForeColor = Color.Red;
                    seconfPoint.BackColor = Color.Transparent;
                    secondlocx = locx;
                    secondlocy = locy;
                    panel3.Controls.Add(seconfPoint);
                }
                if (clickCount == 3)
                {
                    panel3.Invalidate();
                    twoPoint = true;
                    calculatePoint = false;
                }

            }
            else
            {
                if (calculatePoint == false)
                {
                    calculatCounts += 1;
                    PointF addPoint = new PointF();
                    addPoint.X = locx;
                    addPoint.Y = locy;
                    calculatorPoint.Add(addPoint);

                    if (calculatCounts == 1)
                    {
                        Label add = new Label();
                        add.Width = 20;
                        add.Height = 20;
                        add.Text = calculatCounts.ToString();
                        add.Font = new Font("Arial", 10, FontStyle.Bold);
                        add.Location = new Point(locx, locy);
                        add.ForeColor = Color.Red;
                        add.BackColor = Color.Transparent;
                        panel3.Controls.Add(add);
                        add.BringToFront();
                    }

                    panel3.Invalidate();

                }
            }
        }
        public void calculateArea()
        {
            int num_points = calculatorPoint.Count();
            PointF[] pts = new PointF[num_points + 1];

            calculatorPoint.CopyTo(pts, 0);
            pts[num_points] = calculatorPoint[0];

            for (int i = 0; i < num_points; i++)
            {
                area += (pts[i + 1].X - pts[i].X) * (pts[i + 1].Y + pts[i].Y) / 2;
            }

            calculatorPoint.Add(calculatorPoint[0]);
            panel3.Invalidate();
        }
        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            locx = e.X;
            locy = e.Y;
        }
    }
}
